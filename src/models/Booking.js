const mongoose = require('mongoose')

const BookingSchema = new mongoose.Schema({
    date: String,
    approved: Boolean,
    _user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
    _spot: { type: mongoose.Schema.Types.ObjectId, ref: 'Spot' }
}, { timestamps: true })

module.exports = mongoose.model('Booking', BookingSchema)