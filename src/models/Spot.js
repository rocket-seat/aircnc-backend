const mongoose = require("mongoose");

const SpotSchema = new mongoose.Schema(
  {
    thumbnail: String,
    company: String,
    price: Number,
    techs: [String],
    _user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User"
    }
  },
  { timestamps: true, toJSON: { virtuals: true } }
);

SpotSchema.virtual("thumbnail_url").get(function() {
  return `https://aircnc-backend-nodejs.herokuapp.com/files/${this.thumbnail}`;
});

module.exports = mongoose.model("Spot", SpotSchema);
