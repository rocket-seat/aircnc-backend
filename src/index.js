const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const path = require("path");
const socket = require("socket.io");
const http = require("http");

mongoose.connect("mongodb+srv://usuario:senha@banco-vkkfl.mongodb.net/test?retryWrites=true&w=majority", { useNewUrlParser: true, useUnifiedTopology: true });

const app = express();
const server = http.Server(app);
const io = socket(server);

const connectedUsers = {};

io.on("connection", ret => {
  const { user_id } = ret.handshake.query;
  connectedUsers[user_id] = ret.id; // id da conexao do socket
});

app.use((req, res, next) => {
  req.io = io;
  req.connectedUsers = connectedUsers;
  return next();
});

app.use(cors());
app.use(express.json());
app.use("/files", express.static(path.resolve(__dirname, "..", "uploads")));
app.use("/api", require("./routes"));

server.listen(process.env.PORT || 3333);
