const Booking = require('../models/Booking')

module.exports = {
    async store(req, res) {
        const { user_id } = req.headers
        const { spot_id } = req.params
        const { date } = req.body

        const booking = await Booking.create({
            _user: user_id,
            _spot: spot_id,
            date
        })

        await booking.populate('_spot').populate('_user').execPopulate()

        const ownerSocket = req.connectedUsers[booking._spot._user]

        if (ownerSocket) {
            req.io.to(ownerSocket).emit('booking_req', booking)
        }

        return res.json(booking)
    },

}