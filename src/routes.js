const express = require('express')
const multer = require('multer')
const routes = express.Router()

const uploadConfig = require('./config/upload')

const SessionController = require('./controllers/SessionController')
const SpotController = require('./controllers/SpotController')
const DashboardController = require('./controllers/DashboardController')
const BookingController = require('./controllers/BookingController')
const ApprovalController = require('./controllers/ApprovalController')
const RejectionController = require('./controllers/RejectionController')

const upload = multer(uploadConfig)

routes.get('/', (req, res) => {
    return res.json('Rodando na porta 3333...')
})

routes.post('/sessions', SessionController.store)
routes.get('/sessions', SessionController.index)
routes.delete('/sessions/:id', SessionController.destroy)

routes.post('/spots', upload.single('thumbnail'), SpotController.store)
routes.get('/spots', SpotController.index)
routes.delete('/spots/:id', SpotController.destroy)
routes.post('/spots/:spot_id/bookings', BookingController.store)

routes.get('/dashboard', DashboardController.show)

routes.post('/bookings/:booking_id/approvals', ApprovalController.store)
routes.post('/bookings/:booking_id/rejections', RejectionController.store)

module.exports = routes